package smartballapps.com.remoteradargun;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


public class MainMenu extends ActionBarActivity
{
    private boolean receiving;
    private String filename;
    private ArrayList<String> lastSpeeds;
    int lastSpeedsIndex;

    private TextView mphView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
//        if (savedInstanceState == null)
//        {
//            getSupportFragmentManager().beginTransaction()
//                    .add(R.id.container, new PlaceholderFragment())
//                    .commit();
//        }

        receiving=false;
        lastSpeeds=new ArrayList<String>();
        lastSpeedsIndex=0;

        mphView=(TextView)findViewById(R.id.mphView);

        findViewById(R.id.startButton).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (!receiving)
                {
                    startReceiving();
                }

                else
                {
                    receiving=false;
                }
            }
        });
    }

    private void startReceiving()
    {
        receiving=true;
        filename=Integer.toString(new GregorianCalendar().get(Calendar.MINUTE));

        ReceiveTCPMessageTask receive=new ReceiveTCPMessageTask();
        try
        {
            receive.execute();
        }

        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment
    {

        public PlaceholderFragment()
        {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState)
        {
            View rootView = inflater.inflate(R.layout.fragment_main_menu, container, false);
            return rootView;
        }
    }

//    private void sendStartMessage()
//    {
//        new SendTCPPingTask().execute("Testing123");
//    }

    /* Checks if external storage is available for read and write */
    private boolean isExternalStorageWritable()
    {
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state))
        {
            return true;
        }

        return false;
    }

    private File getAlbumStorageDir(Context context, String fileName)
    {
        // Get the directory for the app's private pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), fileName);

        if (!file.mkdirs())
        {
            Log.e("REMOTE_RADAR_GUN", "Directory not created");
        }

        return file;
    }

    private void writeToCSVFile(String newData)
    {
        if (isExternalStorageWritable())
        {
            File file=getAlbumStorageDir(this,filename);

            try
            {
                BufferedWriter writer=new BufferedWriter(new FileWriter(new File(file.getPath() + "/" + filename + ".txt"), true));
                writer.write(new Date().getTime() + "," + newData + "\n");
                writer.close();
            }

            catch (IOException ioex)
            {
                ioex.printStackTrace();
            }
        }
    }

    private class ReceiveTCPMessageTask extends AsyncTask<Void, Void, Boolean>
    {
        protected Boolean doInBackground(Void... views)
        {
            while (receiving)
            {
                try
                {
                    ServerSocket server=new ServerSocket(2194);
                    System.out.println("ABOUT TO LISTEN");
                    Socket socket=server.accept();
                    BufferedReader in=new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    final String newMph=in.readLine();
                    socket.close();
                    in.close();

                    runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            mphView.setText(newMph);
                        }
                    });
                    writeToCSVFile(newMph);
                }

                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            return true;
        }
    }

//    private class SendTCPPingTask extends AsyncTask<String, Void, Boolean>
//    {
//        protected Boolean doInBackground(String... messages)
//        {
//            try
//            {
//                Socket socket=new Socket("192.168.42.1", 2194);
//                OutputStream out=socket.getOutputStream();
//                PrintWriter outwriter=new PrintWriter(out);
//                outwriter.println(messages[0]);
//                outwriter.flush();
//                outwriter.close();
//                socket.close();
//            }
//
//            catch (Exception e)
//            {
//                e.printStackTrace();
//
//                return false;
//            }
//
//            return true;
//        }
//    }
}
